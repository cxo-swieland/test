                            {foreach $sArticle.connectedProducts as $connectedProduct}
                                <li  {if $connectedProduct.attributes.core}data-headline="{$connectedProduct.attributes.core->get('attr2')}" class="js-connected-product {if $sArticle.articleID == $connectedProduct.articleID}active{/if}"{/if}>
                                    <a href="{$connectedProduct.linkDetails}" title="{$connectedProduct.description}">
                                        {if $connectedProduct.attributes.core}
                                            {$color = $connectedProduct.attributes.core->get('attr3')}
                                            {if $color}
                                                <div class="color-bg" style="background-color: {$color};"></div>
                                            {/if}
                                        {/if}
                                    </a>
                                </li>
                            {/foreach}
